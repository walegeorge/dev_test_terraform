terraform {
  required_version = ">= 0.12"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.69.0"
    }
  }
}

provider "aws"{
    shared_credentials_file = "/Users/ITADMIN/.aws/credentials"
    profile = "default"
    region  = "us-west-2" 
}