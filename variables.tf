variable "availability_zones" {
  type        = list(string)
  default     = null
}

variable "my_subnet1" {
  
  default     = "172.16.0.0/24"

}

variable "my_subnet2" {
  
  default     = "172.16.1.0/24"

}