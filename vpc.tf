module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"
  
  name            = "my_vpc"
  cidr            = "172.16.0.0/16"
  
  #azs             = ["us-west-2a", "us-west-2b"]
  #private_subnets = ["172.16.0.0/24","172.16.1.0/24"]
}


resource "aws_route_table" "subnet1_route" {
    vpc_id = module.vpc.vpc_id
    
    route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
   
    tags = {
    "Terraform":"true"
  }
}

resource "aws_subnet" "subnet1"{
    vpc_id     = module.vpc.vpc_id
    availability_zone = "us-west-2a"
    #cidr_block = module.vpc.private_subnets.0
    cidr_block = var.my_subnet1

     tags = {
     Name = "My test subnet"
    "Terraform":"true"
  }
}

resource "aws_subnet" "subnet2"{
    vpc_id     = module.vpc.vpc_id
    availability_zone = "us-west-2b"
    cidr_block = var.my_subnet2
    #cidr_block = module.vpc.private_subnets.1

     tags = {
     Name = "My test subnet"
    "Terraform":"true"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = module.vpc.vpc_id

   tags = {
    Name = "My test internet-gateway"
    "Terraform":"true"
  }
}

resource "aws_main_route_table_association" "subnet1mainroutetable" {
  vpc_id = module.vpc.vpc_id
  route_table_id = aws_route_table.subnet1_route.id
}