resource "aws_launch_template" "web_server" {
  name_prefix               = "web-tests"
  image_id                  = "ami-0f129ae03ee4c74a0"
  instance_type             = "t2.micro"

network_interfaces {
    associate_public_ip_address = true
  }

 tags = {
    Name = "Nginx webserver"
    "Terraform" : "true"
  }
}

resource "aws_autoscaling_group" "web_servers" {
  #availability_zones   = ["us-west-2a", "us-west-2b"]
  availability_zones   = var.availability_zones
  desired_capacity     = 2
  max_size             = 2
  min_size             = 2
  vpc_zone_identifier  = [aws_subnet.subnet1.id, aws_subnet.subnet2.id]

  launch_template {
    id      = aws_launch_template.web_server.id
    version = "$Latest"
  }
  tag {
    key                 = "Name"
    value               = "Nginx webserver"
    propagate_at_launch = true
  }
  tag {
    key                 = "Terraform" 
    value               = "true"
    propagate_at_launch = true
  }
}